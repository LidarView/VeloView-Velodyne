// Copyright 2013 Velodyne Acoustics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#ifndef LQVELOVIEWMANAGER_H
#define LQVELOVIEWMANAGER_H

#include <lqLidarCoreManager.h>

#include "applicationui_export.h"

class APPLICATIONUI_EXPORT lqVeloViewManager : public lqLidarCoreManager
{

  Q_OBJECT
  typedef lqLidarCoreManager Superclass;

public:

  lqVeloViewManager(QObject* parent = nullptr);
  ~lqVeloViewManager() override;

  /**
   * Returns the lqVeloViewManager instance. If no lqVeloViewManager has been
   * created then return nullptr.
   */
  static lqVeloViewManager* instance()
  {
    return qobject_cast<lqVeloViewManager*>(Superclass::instance());
  }

  // VeloView specific
  void pythonStartup() override;

};

#endif // LQVELOVIEWMANAGER_H
